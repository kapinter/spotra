/*
BSD License with a blessing:
May you use this application and code for good!
May you become stronger and healthier and may you have a better life!
*/
import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0
import harbour.spotra 1.0

Page {
    id: page
    property bool paused:false
    SpotraEngine{
        id:spotraengine
        onGpsstrengthChanged: {
            //4 satelites are minimum and 12 or above is the max
            var gps=gpsstrength-3>0?gpsstrength-3:0;
            gpspb.value=(gps/12)>1.0?1.0:gps/12
            //console.log(gps+" "+gpspb.value+" "+gpsstrength+" "+gps/12)
            gpspb.label=gpsstrength+" satelites are used"
            gpssearchlabel.text=gpsstrength+"/4 GPS satelites found."
            if(gpsstrength>3)
                welcomedialog.visible=false
        }
        onSpeedChanged: {
            //console.log(speed);
            speeddigits.speed=speed;
        }
        onDistanceChanged: {
            //console.log(distance)
            distancedigits.distance=distance;
            coverpage.distance=distance.toFixed(1)+" km"
        }
        onAvgspeedChanged: {
            avgspeeddigits.speed=avgspeed;
        }
    }
    Rectangle{
        id:welcomedialog
        color: Theme.secondaryHighlightColor
        border.color: Theme.primaryColor
        border.width: 2
        radius: 8
        anchors.fill: parent
        anchors.margins: 30
        z:10
        Column{
            anchors.fill: parent
            Item{
                width:parent.width
                height: parent.height/7
            }
            Label{
                text: "Welcome to Spotra!"
                anchors.horizontalCenter: parent.horizontalCenter
                height: parent.height/7
                font.pixelSize: Theme.fontSizeLarge
            }
            Item{
                width:parent.width
                height: parent.height/7
            }
            Label{
                text: "Looking for GPS Signal..."
                anchors.horizontalCenter: parent.horizontalCenter
                height: parent.height/7
                font.pixelSize: Theme.fontSizeLarge
            }
            Item{
                width:parent.width
                height: parent.height/7
            }
            Label{
                id:gpssearchlabel
                anchors.horizontalCenter: parent.horizontalCenter
                text: "0/4 GPS satelites found."
                height: parent.height/7
                font.pixelSize: Theme.fontSizeLarge
                wrapMode: Text.WordWrap
            }
            Item{
                width:parent.width
                height: parent.height/7
            }
        }
    }

    Rectangle{
        id:savedialog
        color: Theme.secondaryHighlightColor
        border.color: Theme.primaryColor
        border.width: 2
        radius: 8
        anchors.fill: parent
        anchors.margins: 30
        z:10
        visible: false
        Column{
            anchors.fill: parent
            Item{
                width:parent.width
                height: parent.height/5
            }
            Label{
                text: "Move saved in"
                anchors.horizontalCenter: parent.horizontalCenter
                height: parent.height/5
                font.pixelSize: Theme.fontSizeLarge
            }
            Item{
                width:parent.width
                height: parent.height/5
            }
            Label{
                id:savetext
                text: "Documents folder!"
                anchors.horizontalCenter: parent.horizontalCenter
                height: parent.height/5
                font.pixelSize: Theme.fontSizeLarge
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }
            Item{
                width:parent.width
                height: parent.height/5
            }
        }
        onVisibleChanged: {
            if(savedialog.visible)
                savetimer.start();
        }

        Timer{
            id:savetimer
            repeat: false
            interval: 3000
            running: false
            onTriggered: savedialog.visible=false
        }
    }

    Column{
        spacing: page.height*0.01
        ComboBox {
            width: page.width
            label: "Activity :"

            menu: ContextMenu {
                MenuItem { text: "Running" }
                MenuItem { text: "Biking" }
                MenuItem { text: "Hiking" }
                MenuItem { text: "Walking" }
                MenuItem { text: "Climbing" }
            }
        }
        Label{
            width: page.width
            //height: page.height*0.12
            text: "Distance:"
        }
        DistanceDigits{
            id:distancedigits
            width: page.width
            height: page.height*0.08
        }
        Label{
            width: page.width
            //height: page.height*0.12
            text: "Time:"
        }
        TimeDigits{
            id:timedigits
            width: page.width
            height: page.height*0.08
        }

        Label{
            width: page.width
            //height: page.height*0.12
            text: "Speed:"
        }
        SpeedDigits{
            id:speeddigits
            width: page.width
            height: page.height*0.08
        }
        Label{
            width: page.width
            //height: page.height*0.12
            text: "Avg. Speed:"
        }
        SpeedDigits{
            id:avgspeeddigits
            width: page.width
            height: page.height*0.08
        }
        Label{
            width: page.width
            //height: page.height*0.12
            text: "GPS signal strength:"
        }
        ProgressBar{
            id:gpspb
            width: page.width
            height: page.height*0.1
            value:0
        }
        Item{
            width: page.width
            height: page.height*0.12
            Button{
                id:startbutton
                width: parent.width
                height: parent.height
                text:"START"
                x:0
                y:0
                visible:true
                onPressed: {
                    startbutton.visible=false;
                    pausebutton.visible=true;
                    stopbutton.visible=true;
                    restartbutton.visible=false;
                    //timetimer.starttime=new Date().getTime();
                    timetimer.start();
                    spotraengine.startGPS();
                }
            }
            Button{
                id:pausebutton
                width: parent.width/2
                height: parent.height
                text:"PAUSE"
                visible:false
                x:0
                y:0
                onPressed: {
                    page.paused = true;
                    startbutton.visible=false;
                    pausebutton.visible=false;
                    stopbutton.visible=true;
                    restartbutton.visible=true;
                    timetimer.stop();
                    spotraengine.pauseGPS();
                    speeddigits.speed=0;
                }
            }
            Button{
                id:restartbutton
                width: parent.width/2
                height: parent.height
                text:"Restart"
                visible:false
                x:0
                y:0
                onPressed: {
                    page.paused = false;
                    startbutton.visible=false;
                    pausebutton.visible=true;
                    stopbutton.visible=true;
                    restartbutton.visible=false;
                    //timetimer.starttime=new Date().getTime();
                    timetimer.start();
                    spotraengine.restartGPS();
                }
            }
            Button{
                id: stopbutton
                width: parent.width/2
                height: parent.height
                text:"STOP"
                visible: false
                x:parent.width/2
                y:0
                onPressed: {
                    page.paused = true;
                    startbutton.visible=true;
                    pausebutton.visible=false;
                    stopbutton.visible=false;
                    restartbutton.visible=false;
                    timetimer.stop();
                    //QString filename("");
                    //filename.append(QDateTime::currentDateTime().toString("dd_MM_yyyy_hh_mm_ss"));
                    //filename.append(".gpx");
                    /*var curtime = date.getTime();
                    var locale= Qt.locale()
                    property string dateTimeString: "Tue 2013-09-17 10:56:06"

                    Component.onCompleted: {
                        print(Date.fromLocaleString(locale, dateTimeString, "dd_MM_yyyy_hh_mm_ss"));*/
                    //savetext.text=""+Date().toLocaleString(Qt.locale(),"dd_MM_yyyy_hh_mm_ss")+".gpx";
                    savedialog.visible=true;
                    spotraengine.stopGPS();
                    spotraengine.saveGPX();
                    timedigits.hour=0;
                    timedigits.min=0;
                    timedigits.sec=0;
                    speeddigits.speed=0;
                }
            }
            Timer{
                id:timetimer
                repeat: true
                interval: 1000
                running: false
                /*property var starttime: 0
                property var difftime: 0
                property var date: new Date();*/
                onTriggered: {
                    //the timer is not accurate so it becomes late
                    /*if(timedigits.sec===59) {
                        timedigits.sec=0;
                        if(timedigits.min===59) {
                            timedigits.min=0;
                            if(timedigits.hour===24)
                                timedigits.hour=0;
                            else
                                timedigits.hour++;
                        }
                        else
                            timedigits.min++;
                    }
                    else
                        timedigits.sec++;*/
                    //var curtime = date.getTime();
                    /*console.log(date.getTime())
                    difftime = date.getTime() - starttime;
                    console.log(starttime);
                    console.log(difftime);
                    timedigits.hour=Math.floor(difftime/360000);
                    timedigits.min=Math.floor((difftime-(timedigits.hour*360000))/60000);
                    timedigits.sec=Math.floor((difftime-(timedigits.hour*360000)-(timedigits.min*60000))/1000);*/
                    spotraengine.trackTime();
                    timedigits.hour=spotraengine.getHours();
                    timedigits.min=spotraengine.getMins();
                    timedigits.sec=spotraengine.getSecs();
                    coverpage.time=timedigits.hour+":"+timedigits.min+":"+timedigits.sec;
                }
            }
        }
    }
    Timer{
        id: dimtimer
        repeat: true
        interval: 2500
        running: false
        onTriggered: {
            if(!page.paused)
                dimmanager.sendDbusMessage();
        }
    }

    Component.onCompleted: {
        dimtimer.start();
    }

    Connections {
        target: Qt.application
        onActiveChanged: {
            if(!Qt.application.active)
                dimtimer.stop();
            else
                dimtimer.start();
        }

    }
}


