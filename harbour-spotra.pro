# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-spotra

CONFIG += sailfishapp

SOURCES += src/harbour-spotra.cpp \
    src/dimmanager.cpp \
    src/spotraengine.cpp

OTHER_FILES += qml/harbour-spotra.qml \
    qml/CoverPage.qml \
    qml/MainPage.qml \
    qml/Digit.qml \
    rpm/harbour-spotra.changes.in \
    rpm/harbour-spotra.spec \
    rpm/harbour-spotra.yaml \
    translations/*.ts \
    harbour-spotra.desktop \
    qml/DistanceDigits.qml \
    qml/TimeDigits.qml \
    qml/SpeedDigits.qml

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n
QT += positioning dbus core
# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
#TRANSLATIONS += translations/harbour-spotra-de.ts

HEADERS += \
    src/dimmanager.h \
    src/spotraengine.h

