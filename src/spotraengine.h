/*
BSD License with a blessing:
May you use this application and code for good!
May you become stronger and healthier and may you have a better life!
*/
#ifndef SPOTRAENGINE_H
#define SPOTRAENGINE_H

#include <QQuickItem>
#include <QtPositioning>
#include <QTimer>
#include <QDateTime>

//gps signal strength, distance, speed, avg speed
//later hrm can be added
class SpotraEngine : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(int gpsstrength READ gpsstrength NOTIFY gpsstrengthChanged)
    Q_PROPERTY(double speed READ speed NOTIFY speedChanged)
    Q_PROPERTY(double avgspeed READ avgspeed NOTIFY avgspeedChanged)
    Q_PROPERTY(double distance READ distance NOTIFY distanceChanged)

public:
    explicit SpotraEngine(QQuickItem *parent = 0);
    int gpsstrength() const;
    qreal speed() const;
    qreal avgspeed() const;
    qreal distance() const;
    Q_INVOKABLE void startGPS();
    Q_INVOKABLE void pauseGPS();
    Q_INVOKABLE void restartGPS();
    Q_INVOKABLE void stopGPS();
    Q_INVOKABLE void saveGPX();
    Q_INVOKABLE int getSecs();
    Q_INVOKABLE int getMins();
    Q_INVOKABLE int getHours();
    Q_INVOKABLE void trackTime();
signals:
    void gpsstrengthChanged();
    void speedChanged();
    void avgspeedChanged();
    void distanceChanged();
public slots:
    // This slot is invoked whenever new location information are retrieved
    void positionUpdated(const QGeoPositionInfo & pos);
    // This slot is invoked whenever a timeout happend while retrieving location information
    void positionUpdateTimeout();
    // This slot is invoked whenever new information about the used satellites are retrieved
    void satellitesInUseUpdated(const QList<QGeoSatelliteInfo> & satellites);
    // This slot is invoked whenever new information about the in-view satellites are retrieved
    void satellitesInViewUpdated(const QList<QGeoSatelliteInfo> & satellites);
    void updateTestValues();
private:
    int m_gpsstrength;//4-12 is usable
    qreal m_speed;
    qreal m_avgspeed;
    qreal m_distance;
    QGeoPositionInfoSource *m_positionSource;
    QGeoSatelliteInfoSource *m_satelliteSource;
    QTimer m_testTimer;
    QList<double> m_longitudeList;
    QList<double> m_latitudeList;
    QList<QDateTime> m_timestampList;
    QList<double> m_altitudeList;
    QList<int> m_lapindexList;
    bool m_running;
    QXmlStreamWriter m_xml;
    //avarage calculation
    double m_longtmp;
    double m_lattmp;
    double m_alttmp;
    double m_speedtmp;
    int m_count;
    qint64 m_starttime;
    qint64 m_restarttime;
    int m_secs;
    int m_mins;
    int m_hours;
};

#endif // SPOTRAENGINE_H
