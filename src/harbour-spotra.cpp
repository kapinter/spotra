/*
BSD License with a blessing:
May you use this application and code for good!
May you become stronger and healthier and may you have a better life!
*/

//#ifdef QT_QML_DEBUG
#include <QtQuick>
//#endif

#include <sailfishapp.h>
#include "dimmanager.h"
#include "spotraengine.h"

/*int main(int argc, char *argv[])
{
    // SailfishApp::main() will display "qml/template.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //
    // To display the view, call "show()" (will show fullscreen on device).

    return SailfishApp::main(argc, argv);
}*/

int main(int argc, char *argv[])
{
    QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));
    QScopedPointer<QQuickView> view(SailfishApp::createView());
    QScopedPointer<DimManager> dimManager(new DimManager());
    view->rootContext()->setContextProperty("dimmanager", dimManager.data());
    /*QScopedPointer<SpotraEngine> spotraEngine(new SpotraEngine());
    view->rootContext()->setContextProperty("spotraengine", spotraEngine.data());*/
    qmlRegisterType<SpotraEngine>("harbour.spotra", 1, 0, "SpotraEngine");
    view->setSource(SailfishApp::pathTo("qml/harbour-spotra.qml"));
    view->showFullScreen();
    return app->exec();
}
