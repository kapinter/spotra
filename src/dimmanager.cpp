/*
BSD License with a blessing:
May you use this application and code for good!
May you become stronger and healthier and may you have a better life!
*/
#include "dimmanager.h"
#include <QtDBus/QtDBus>

DimManager::DimManager(QQuickItem *parent) :
    QQuickItem(parent)
{
}

void DimManager::sendDbusMessage()
{
    QDBusConnection systemBus = QDBusConnection::connectToBus(QDBusConnection::SystemBus, "system");
    if(systemBus.isConnected()) {
        QDBusMessage msg = QDBusMessage::createMethodCall("com.nokia.mce", "/com/nokia/mce/request",
                                                          "com.nokia.mce.request", "req_display_state_on");
        (void)systemBus.call(msg);
    }
}

