/*
BSD License with a blessing:
May you use this application and code for good!
May you become stronger and healthier and may you have a better life!
*/
#ifndef DIMMANAGER_H
#define DIMMANAGER_H

#include <QQuickItem>

class DimManager : public QQuickItem
{
    Q_OBJECT
public:
    explicit DimManager(QQuickItem *parent = 0);
    Q_INVOKABLE void sendDbusMessage();
signals:

public slots:
};

#endif // DIMMANAGER_H
